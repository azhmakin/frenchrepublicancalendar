//
// Copyright (c) an 225 (2017 CE) Andrey Zhmakin
//

package zhmakin.french_republican_calendar;


import org.junit.Assert;
import org.junit.Test;

import java.util.GregorianCalendar;
import java.util.TimeZone;


/**
 * Tests the calendar implementation.
 * @author Andrey Zhmakin
 */
public class FrenchRepublicanCalendarTest
{
    public static int[][] DATES =
        {
            { 1, FrenchRepublicanCalendar.VENDEMIAIRE,  1, 1792, GregorianCalendar.SEPTEMBER, 22}, // Beginning of the New Era
            { 2, FrenchRepublicanCalendar.PLUVIOSE   ,  4, 1794, GregorianCalendar.JANUARY  , 23}, // Liberty tree planting law
            { 2, FrenchRepublicanCalendar.PRAIRIAL   , 20, 1794, GregorianCalendar.JUNE     ,  8}, // First Festival of the Supreme Being
            { 2, FrenchRepublicanCalendar.PRAIRIAL   , 22, 1794, GregorianCalendar.JUNE     , 10}, // Law of 22 Prairial
            { 2, FrenchRepublicanCalendar.THERMIDOR  ,  9, 1794, GregorianCalendar.JULY     , 27}, // Thermidorian Reaction
            { 3, FrenchRepublicanCalendar.BRUMAIRE   ,  9, 1794, GregorianCalendar.OCTOBER  , 30}, // École Normale Supérieure established
            { 3, FrenchRepublicanCalendar.NIVOSE     ,  8, 1794, GregorianCalendar.DECEMBER , 28}, // Law on reorganisation of the Revolutionary Tribunal
            { 3, FrenchRepublicanCalendar.GERMINAL   , 12, 1795, GregorianCalendar.APRIL    ,  1}, // Insurrection of 12 Germinal Year III
            { 3, FrenchRepublicanCalendar.PRAIRIAL   ,  1, 1795, GregorianCalendar.MAY      , 20}, // Revolt of 1 Prairial Year III
            { 3, FrenchRepublicanCalendar.PRAIRIAL   , 12, 1795, GregorianCalendar.MAY      , 31}, // Suppression of the Revolutionary Tribunal
            { 3, FrenchRepublicanCalendar.FLOREAL    , 17, 1795, GregorianCalendar.MAY      , 6 }, // Conviction of Fouquier-Tinville
            { 3, FrenchRepublicanCalendar.FLOREAL    , 18, 1795, GregorianCalendar.MAY      , 7 }, // Execution of Fouquier-Tinville
            { 3, FrenchRepublicanCalendar.FRUCTIDOR  ,  5, 1795, GregorianCalendar.AUGUST   , 22}, // Constitution de la République Française
            { 4, FrenchRepublicanCalendar.FLOREAL    , 22, 1796, GregorianCalendar.MAY      , 11}, // Alleged date of the Conspiracy of the Equals
            { 8, FrenchRepublicanCalendar.BRUMAIRE   , 18, 1799, GregorianCalendar.NOVEMBER ,  9}, // Napoleon Bonaparte coup
            {13, FrenchRepublicanCalendar.FRIMAIRE   , 11, 1804, GregorianCalendar.DECEMBER ,  2}, // Coronation of Napoleon I
            {13, FrenchRepublicanCalendar.FRUCTIDOR  , 22, 1805, GregorianCalendar.SEPTEMBER,  9}, // Abolition of the calendar
            {79, FrenchRepublicanCalendar.FLOREAL    , 16, 1871, GregorianCalendar.MAY      ,  6}, // Start of use by the Paris Commune
            {79, FrenchRepublicanCalendar.PRAIRIAL   ,  3, 1871, GregorianCalendar.MAY      , 23}, // End of use by the Paris Commune
        };


    @Test
    public void testEstablishedLeapYears()
    {
        FrenchRepublicanCalendar calendar = new FrenchRepublicanCalendar();

        Assert.assertTrue(calendar.isLeapYear( 3));
        Assert.assertTrue(calendar.isLeapYear( 7));
        Assert.assertTrue(calendar.isLeapYear(11));

        Assert.assertFalse(calendar.isLeapYear( 1));
        Assert.assertFalse(calendar.isLeapYear( 2));
        Assert.assertFalse(calendar.isLeapYear( 4));
        Assert.assertFalse(calendar.isLeapYear( 5));
        Assert.assertFalse(calendar.isLeapYear( 6));
        Assert.assertFalse(calendar.isLeapYear( 8));
        Assert.assertFalse(calendar.isLeapYear( 9));
        Assert.assertFalse(calendar.isLeapYear(10));
        Assert.assertFalse(calendar.isLeapYear(12));
    }


    @Test
    public void testProposedLeapYears()
    {
        FrenchRepublicanCalendar calendar = new FrenchRepublicanCalendar();

        Assert.assertTrue(calendar.isLeapYear(15));
        Assert.assertTrue(calendar.isLeapYear(20));

        Assert.assertFalse(calendar.isLeapYear(13));
        Assert.assertFalse(calendar.isLeapYear(14));
        Assert.assertFalse(calendar.isLeapYear(16));
        Assert.assertFalse(calendar.isLeapYear(17));
        Assert.assertFalse(calendar.isLeapYear(18));
        Assert.assertFalse(calendar.isLeapYear(19));
        Assert.assertFalse(calendar.isLeapYear(21));
    }


    @Test
    public void testImportantDates()
    {
        for (int[] datePair : DATES)
        {
            int frYear  = datePair[0];
            int frMonth = datePair[1];
            int frDay   = datePair[2];

            int grYear  = datePair[3];
            int grMonth = datePair[4];
            int grDay   = datePair[5];

            GregorianCalendar gregorianCalendar = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
            gregorianCalendar.set(grYear, grMonth, grDay);

            FrenchRepublicanCalendar frenchCalendar
                = new FrenchRepublicanCalendar(gregorianCalendar.getTimeInMillis());

            Assert.assertTrue(frYear + "/" + frMonth + "/" + frDay,
                frenchCalendar.get(FrenchRepublicanCalendar.YEAR) == frYear
                && frenchCalendar.get(FrenchRepublicanCalendar.MONTH) == frMonth
                && frenchCalendar.get(FrenchRepublicanCalendar.DAY_OF_MONTH) == frDay);
        }
    }
}
