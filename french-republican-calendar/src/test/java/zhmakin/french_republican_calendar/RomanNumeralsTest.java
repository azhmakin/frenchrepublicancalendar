//
// Copyright (c) 2017 Andrey Zhmakin
//

package zhmakin.french_republican_calendar;

import org.junit.Assert;
import org.junit.Test;

import static zhmakin.french_republican_calendar.RomanNumerals.parseRoman;
import static zhmakin.french_republican_calendar.RomanNumerals.toRoman;

/**
 * Tests Roman numerals converter.
 * @author Andrey Zhmakin
 */
public class RomanNumeralsTest
{
    @Test
    public void test_01()
    {
        for (int i = 1; i <= 3999; i++)
        {
            Assert.assertTrue("Offending number: " + i, parseRoman(toRoman(i)) == i);
        }
    }


    @Test
    public void test_02()
    {
        Assert.assertTrue(parseRoman("MMMCMXCIX") == 3999);
        Assert.assertTrue(parseRoman("XIIX")      ==   18);
        Assert.assertTrue(parseRoman("IIXX")      ==   18);
        Assert.assertTrue(parseRoman("XVIII")     ==   18);
        Assert.assertTrue(parseRoman("MDCCCCX")   == 1910);
        Assert.assertTrue(parseRoman("MDCDIII")   == 1903);
        Assert.assertTrue(parseRoman("MCMIII")    == 1903);
    }


    @Test
    public void test_03()
    {
        Assert.assertTrue( toRoman( 207) .equals( "CCVII"     ) );
        Assert.assertTrue( toRoman(1066) .equals( "MLXVI"     ) );
        Assert.assertTrue( toRoman(1954) .equals( "MCMLIV"    ) );
        Assert.assertTrue( toRoman(1990) .equals( "MCMXC"     ) );
        Assert.assertTrue( toRoman(2014) .equals( "MMXIV"     ) );
        Assert.assertTrue( toRoman(1910) .equals( "MCMX"      ) );
        Assert.assertTrue( toRoman(1903) .equals( "MCMIII"    ) );
        Assert.assertTrue( toRoman(3999) .equals( "MMMCMXCIX" ) );
    }
}
