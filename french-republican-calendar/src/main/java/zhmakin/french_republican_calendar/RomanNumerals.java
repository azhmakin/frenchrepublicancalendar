//
// Copyright (c) 2017 Andrey Zhmakin
//

package zhmakin.french_republican_calendar;

/**
 * Converts to and from Roman numerals.
 * @author Andrey Zhmakin
 */
public class RomanNumerals
{
    private final static int[]    VALUE          = {  1 ,   4 ,  5 ,   9 ,  10,  40 ,  50,  90 , 100,  400, 500,  900, 1000 };
    private final static String[] REPRESENTATION = { "I", "IV", "V", "IX", "X", "XL", "L", "XC", "C", "CD", "D", "CM",  "M" };


    public static String toRoman(int number)
    {
        assert number >= 1 && number <= 3999;

        StringBuilder builder = new StringBuilder();

        int r = number;

        int topIndex = VALUE.length;

        while (r > 0)
        {
            for (int i = topIndex; --i >= 0; )
            {
                if (r >= VALUE[i])
                {
                    r -= VALUE[i];
                    builder.append(REPRESENTATION[i]);
                    topIndex = i + 1;
                    break;
                }
            }
        }

        assert r == 0;

        return builder.toString();
    }


    public static int parseRoman(String roman)
    {
        int result = 0;

        int prev = Integer.MAX_VALUE;
        int r = 0;

        for (char c : roman.trim().toCharArray())
        {
            int curr = valueForSymbol(c);

            if (curr == 0)
            {
                throw new NumberFormatException("Not a valid Roman numeral: " + roman);
            }

            if (curr > prev)
            {
                result += curr - r;
                r = 0;
            }
            else if (curr < prev)
            {
                result += r;
                r = curr;
            }
            else
            {
                r += curr;
            }

            prev = curr;
        }

        result += r;

        return result;
    }


    private static int valueForSymbol(char symbol)
    {
        switch (symbol)
        {
            case 'I': return 1;
            case 'V': return 5;
            case 'X': return 10;
            case 'L': return 50;
            case 'C': return 100;
            case 'D': return 500;
            case 'M': return 1000;
            default : return 0;
        }
    }
}
