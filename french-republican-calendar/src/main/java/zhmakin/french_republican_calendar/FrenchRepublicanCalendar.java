//
// Copyright (c) an 225 (2017 CE) Andrey Zhmakin
//

package zhmakin.french_republican_calendar;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

/**
 * <code>FrenchRepublicanCalendar</code> is a subclass of the <code>Calendar</code> and
 * implements a calendar introduced after the French revolution.
 * @author Andrey Zhmakin
 */
public class FrenchRepublicanCalendar
    extends java.util.Calendar
{
    public final static int VENDEMIAIRE    = 0;
    public final static int BRUMAIRE       = 1;
    public final static int FRIMAIRE       = 2;
    public final static int NIVOSE         = 3;
    public final static int PLUVIOSE       = 4;
    public final static int VENTOSE        = 5;
    public final static int GERMINAL       = 6;
    public final static int FLOREAL        = 7;
    public final static int PRAIRIAL       = 8;
    public final static int MESSIDOR       = 9;
    public final static int THERMIDOR      = 10;
    public final static int FRUCTIDOR      = 11;
    public final static int SANSCULOTTIDES = 12;

    public final static int PRIMIDI  = 1;
    public final static int DUODI    = 2;
    public final static int TRIDI    = 3;
    public final static int QUARTIDI = 4;
    public final static int QUINTIDI = 5;
    public final static int SEXTIDI  = 6;
    public final static int SEPTIDI  = 7;
    public final static int OCTIDI   = 8;
    public final static int NONIDI   = 9;
    public final static int DECADI   = 10;

    public final static int CELEBRATION_OF_VIRTUE       = 361;
    public final static int CELEBRATION_OF_TALENT       = 362;
    public final static int CELEBRATION_OF_LABOUR       = 363;
    public final static int CELEBRATION_OF_CONVICTIONS  = 364;
    public final static int CELEBRATION_OF_HONORS       = 365;
    public final static int CELEBRATION_OF_REVOLUTION   = 366;

    public final static int ANCIEN_REGIME  = 0;
    public final static int REPUBLICAN_ERA = 1;

    public final static int DAY_OF_DECADE   = Calendar.FIELD_COUNT;
    public final static int DECADE_OF_MONTH = Calendar.FIELD_COUNT + 1;
    public final static int DECADE_OF_YEAR  = Calendar.FIELD_COUNT + 2;
    public final static int DECIMAL_HOUR    = Calendar.FIELD_COUNT + 3;
    public final static int DECIMAL_MINUTE  = Calendar.FIELD_COUNT + 4;
    public final static int DECIMAL_SECOND  = Calendar.FIELD_COUNT + 5;


    public final static String[] MONTH_NAME =
        {
            "Vendemiaire", "Brumaire", "Frimaire", "Nivose", "Pluviose", "Ventose",
            "Germinal", "Floreal", "Prairial", "Messidor", "Thermidor", "Fructidor",
            "Sansculottides"
        };


    public final static long MILLISECONDS_IN_DAY = 24 * 60 * 60 * 1000;

    /**
     * Beginning of the new era (1 Vendemiaire an 1) relative to UNIX epoch.
     */
    private final static long ERA_OFFSET = -64748 * MILLISECONDS_IN_DAY;

    public final static long GREENWICH_PARIS_OFFSET = (9 * 60 + 21) * 1000L;

    public final static double GREENWICH_PARIS_OFFSET_DAYS = GREENWICH_PARIS_OFFSET / (double) MILLISECONDS_IN_DAY;

    protected int[] frFields = new int[DECIMAL_SECOND - DAY_OF_DECADE + 1];

    private LeapRule leapRule =
        LEAP_RULE_EQUINOX_TABLE;
        //LEAP_RULE_EQUINOX_APPROX_CALCULATION;
        //LEAP_RULE_EQUINOX_TABLE;
        //LEAP_RULE_4_128RD;


    public interface LeapRule
    {
        boolean isLeapYear(int year);
    }


    public final static LeapRule LEAP_RULE_EQUINOX_TABLE = LeapRuleEquinoxTable.getInstance();


    public final static LeapRule LEAP_RULE_EQUINOX_APPROX_CALCULATION = LeapRuleApproxCalculation.getInstance();


    public final static LeapRule LEAP_RULE_4_128RD =
        new  LeapRule()
        {
            @Override
            public boolean isLeapYear(int year)
            {
                assert year >= 1;

                return (year + 1) % 4 == 0 && (year + 1) % 128 != 0;
            }
        };


    public final static LeapRule LEAP_RULE_4_128 =
        new  LeapRule()
        {
            @Override
            public boolean isLeapYear(int year)
            {
                assert year >= 1;

                if (year == 19) return false;
                if (year == 20) return true;

                return (year + 1) % 4 == 0 && (year + 1) % 128 != 0;
            }
        };


    public final static LeapRule LEAP_RULE_ROMME =
        new  LeapRule()
        {
            @Override
            public boolean isLeapYear(int year)
            {
                assert year >= 1;

                if (year == 19) return false;
                if (year == 20) return true;

                int y = year + 1;

                if ((y & 3) != 0)
                {
                    return false;
                }

                return (y % 100 != 0) || (y % 400 == 0);
            }
        };


    public FrenchRepublicanCalendar(TimeZone timeZone)
    {
        super(timeZone, Locale.ENGLISH);

        this.time = System.currentTimeMillis();

        this.computeFields();
    }


    public FrenchRepublicanCalendar()
    {
        super();

        this.time = System.currentTimeMillis();

        this.computeFields();
    }


    public FrenchRepublicanCalendar(long millis)
    {
        super();

        this.time = millis;

        this.computeFields();
    }


    public FrenchRepublicanCalendar(int year, int month, int dayOfMonth)
    {
        this.set(YEAR, year);
        this.set(MONTH, month);
        this.set(DAY_OF_MONTH, dayOfMonth);

        //this.updateTime();
        //this.computeFields();
    }


    public boolean isLeapYear(int year)
    {
        return this.leapRule.isLeapYear(year);
    }


    private int daysInYear(int year)
    {
        return this.isLeapYear(year) ? 366 : 365;
    }


    /**
     * Initialise fields from time.
     */
    private void countToDate()
    {
        long relativeTime = this.time - ERA_OFFSET;

        // TODO: Take time zone into account, relativeTime => localTime!

        if (relativeTime < 0)
        {
            this.fields[ERA] = ANCIEN_REGIME;
            return;
        }

        long daysBeforeToday = relativeTime / MILLISECONDS_IN_DAY;

        long daysBeforeThisYear = 0;

        int year = 1;

        while (daysBeforeThisYear + daysInYear(year) <= daysBeforeToday)
        {
            daysBeforeThisYear += daysInYear(year);
            year++;
        }

        int dayOfYear = (int) (daysBeforeToday - daysBeforeThisYear);

        assert !isLeapYear(year) || dayOfYear < 366;
        assert !!isLeapYear(year) || dayOfYear < 365;

        int month = dayOfYear / 30;
        int dayOfMonth = dayOfYear % 30 + 1;

        this.fields[ERA] = REPUBLICAN_ERA;

        this.fields[YEAR] = year;
        this.fields[MONTH] = month;
        this.fields[DAY_OF_MONTH] = dayOfMonth;

        // Set seven-day week related fields

        this.fields[DAY_OF_WEEK] = getDayOfWeek(daysBeforeToday);

        this.fields[WEEK_OF_MONTH] = getWeekOfMonth(dayOfMonth, this.fields[DAY_OF_WEEK]);

        this.fields[WEEK_OF_YEAR] = getWeekOfYear(dayOfYear, this.fields[DAY_OF_WEEK]);

        // Set decade-related fields
        this.setField(DAY_OF_DECADE, getDayOfDecade(dayOfMonth));
        this.setField(DECADE_OF_MONTH, month / 10 + 1);
        this.setField(DECADE_OF_YEAR, month * 3 + dayOfMonth / 10 + 1);

        this.fields[DAY_OF_YEAR] = dayOfYear + 1;

        long millisecondOfDay = relativeTime % MILLISECONDS_IN_DAY;

        assert millisecondOfDay == relativeTime - daysBeforeToday * MILLISECONDS_IN_DAY;

        assert millisecondOfDay >= 0;

        //case DECIMAL_TIME:
        {
            int decimalSeconds = millisecondsToDecimalSeconds(millisecondOfDay);

            assert decimalSeconds >= 0;

            int decimalSecond = decimalSeconds % 100;
            int decimalMinute = (decimalSeconds / 100) % 100;
            int decimalHour = (decimalSeconds / 10000);

            this.setField(DECIMAL_SECOND, decimalSecond);
            this.setField(DECIMAL_MINUTE, decimalMinute);
            this.setField(DECIMAL_HOUR, decimalHour);
        }

        int hour, minute, second, millisecond;

        //case CONVENTIONAL_TIME:
        {
            millisecond = (int) (millisecondOfDay % 1000L);

            second = (int) (millisecondOfDay / 1000L);

            hour = second / (60 * 60);
            second -= hour * 60 * 60;

            minute = second / 60;
            second -= minute * 60;

            this.fields[HOUR_OF_DAY] = hour;
            this.fields[MINUTE] = minute;
            this.fields[SECOND] = second;
            this.fields[MILLISECOND] = millisecond;
        }

        if (hour <= 11)
        {
            this.fields[HOUR] = hour;
            this.fields[AM_PM] = AM;
        }
        else
        {
            this.fields[HOUR] = (hour == 12) ? 12 : (hour - 12);
            this.fields[AM_PM] = PM;
        }
    }


    /**
     * This method updates the <code>time</code> field from the calendar field.
     */
    private void updateTime()
    {
        this.time = 0;

        int year = this.fields[YEAR];

        for (int y = 1; y < year; y++)
        {
            this.time += (365 + (this.isLeapYear(y) ? 1 : 0)) * MILLISECONDS_IN_DAY;
        }

        int month = this.fields[MONTH];

        this.time += month * 30 * MILLISECONDS_IN_DAY;

        int dayOfMonth = this.fields[DAY_OF_MONTH];

        this.time += (dayOfMonth - 1) * MILLISECONDS_IN_DAY;

        //case DECIMAL_TIME:
        {
            /*int hour = this.fields[HOUR_OF_DAY];
            int minute = this.fields[MINUTE];
            int second = this.fields[SECOND];

            int seconds = (hour * 100 + minute) * 100 + second;

            this.time += decimalSecondsToMilliseconds(seconds);*/
        }

        //case CONVENTIONAL_TIME:
        {
            int hour = this.fields[HOUR_OF_DAY];
            int minute = this.fields[MINUTE];
            int second = this.fields[SECOND];
            int millisecond = this.fields[MILLISECOND];

            long milliseconds = ((hour * 60 + minute) * 60 + second) * 1000L + millisecond;

            this.time += milliseconds;
        }

        this.time += ERA_OFFSET;
    }


    private void setField(int field, int value)
    {
        if (field < Calendar.FIELD_COUNT)
        {
            this.fields[field] = value;
        }
        else
        {
            this.frFields[field - Calendar.FIELD_COUNT] = value;
        }
    }


    private int getField(int field)
    {
        return (field < Calendar.FIELD_COUNT) ? this.fields[field] : this.frFields[field - Calendar.FIELD_COUNT];
    }

    //====================================================================================
    // Calendar overridden methods
    //====================================================================================


    @Override
    public int get(int field)
    {
        if (field < Calendar.FIELD_COUNT)
        {
            return super.get(field);
        }
        else
        {
            return this.frFields[field - Calendar.FIELD_COUNT];
        }
    }


    @Override
    public void set(int field, int value)
    {
        if (field < Calendar.FIELD_COUNT)
        {
            super.set(field, value);
        }
        else
        {
            this.frFields[field - Calendar.FIELD_COUNT] = value;
        }

    }


    @Override
    protected void computeTime()
    {
        this.updateTime();
    }


    @Override
    protected void computeFields()
    {
        //this.clear();

        this.countToDate();
    }


    @Override
    public void add(int field, int amount)
    {

    }


    @Override
    public void roll(int field, boolean up)
    {

    }


    @Override
    public int getMinimum(int field)
    {
        switch (field)
        {
            case DAY_OF_MONTH : return 1;
            case MONTH        : return VENDEMIAIRE;
            case DAY_OF_WEEK  : return PRIMIDI;
            case WEEK_OF_MONTH: return 1;
            case WEEK_OF_YEAR : return 1;
            case DAY_OF_YEAR :  return 1;
            //case DAY_OF_WEEK_IN_MONTH
            //case FIE
        }

        return 0;
    }


    @Override
    public int getMaximum(int field)
    {
        switch (field)
        {
            case DAY_OF_MONTH : return 30;
            case MONTH        : return SANSCULOTTIDES;
            case DAY_OF_WEEK  : return DECADI;
            case WEEK_OF_MONTH: return 3;
            case WEEK_OF_YEAR : return 12 * 3 + 1;
            case DAY_OF_YEAR  : return 12 * 30 + 6;
            //case DAY_OF_WEEK_IN_MONTH
            //case FIE

            case DECIMAL_HOUR: return 9;
            case DECIMAL_MINUTE: return 99;
            case DECIMAL_SECOND: return 99;
        }

        return 0;
    }


    @Override
    public int getGreatestMinimum(int field)
    {
        return this.getMinimum(field);
    }


    @Override
    public int getLeastMaximum(int field)
    {
        switch (field)
        {
            case DAY_OF_MONTH : return 6;
            case DAY_OF_YEAR  : return 12 * 30 + 5;
        }

        return this.getMaximum(field);
    }


    /*@Override
    public String getCalendarType()
    {
        return "french";
    }*/

    //====================================================================================


    public String readable()
    {
        //this.computeFields();

        return this.fields[DAY_OF_MONTH]
            + " " + FrenchRepublicanCalendar.MONTH_NAME[this.fields[MONTH]]
            + " " + this.fields[YEAR]
            + " " + this.fields[HOUR_OF_DAY]
            + ":" + new DecimalFormat("00").format(this.fields[MINUTE])
            + ":" + new DecimalFormat("00").format(this.fields[SECOND]);
    }


    //====================================================================================

    /**
     * Converts the day of the era into the day of conventional seven day week.
     * @param dayOfEra Day of the era, 1 Vendemiaire an 1 is day 0.
     * @return Day of the seven day week.
     */
    private static int getDayOfWeek(long dayOfEra)
    {
        // 22 September 1792 (aka 1 Vendemiaire an 1) is Saturday

        return (int)( (dayOfEra + 6) % 7 + 1 );
    }


    /**
     * Returns Week of month.
     * @param dayOfMonth Day of month.
     * @param dayOfWeek 1, 2 - 7 : Sun, Mon - Sat
     * @return Week of month, first week is 1.
     */
    private int getWeekOfMonth(int dayOfMonth, int dayOfWeek)
    {
        // TODO: Replace with arithmetic calculation, get rid of loop!

        int weekCount = 1;

        for (int day = dayOfMonth; day >= 1; day--)
        {
            if (dayOfWeek == this.getFirstDayOfWeek() && day != 1)
            {
                weekCount++;
            }

            if (dayOfWeek == SUNDAY)
            {
                dayOfWeek = SATURDAY;
            }
            else
            {
                dayOfWeek--;
            }
        }

        return weekCount;
    }


    /**
     * Returns week of the year.
     * @param dayOfYear 0-366
     * @param dayOfWeek 1, 2 - 7 : Sun, Mon - Sat
     * @return Week of year, first week is 1.
     */
    private int getWeekOfYear(int dayOfYear, int dayOfWeek)
    {
        // TODO: Replace with arithmetic calculation, get rid of loop!
        // TODO: There are multiple ways to number weeks of year.

        int weekOfYear = 1;

        for (int day = dayOfYear; day >= 0; day--)
        {
            if (dayOfWeek == this.getFirstDayOfWeek()
                && day != 0)
            {
                weekOfYear++;
            }

            if (dayOfWeek == SUNDAY)
            {
                dayOfWeek = SATURDAY;
            }
            else
            {
                dayOfWeek--;
            }
        }

        return weekOfYear;
    }


    private static int getDayOfDecade(int dayOfMonth)
    {
        int reminder = dayOfMonth % 10;

        return reminder == 0 ? 10 : reminder;
    }


    private static int toDecimalSecond(int second)
    {
        assert second >= 0 && second < 24 * 60 * 60 : "second = " + second;

        return (second * 125) / 108;
    }


    private static int millisecondsToDecimalSeconds(long milliseconds)
    {
        return (int) (milliseconds * 125L / 108L / 1000L);
    }


    private static int decimalSecondsToMilliseconds(long decimalSeconds)
    {
        return (int) (decimalSeconds * 108L * 1000L / 125L);
    }


    static int julianToEquinoxDay(double julianDay)
    {
        return (int) Math.round( Math.floor(julianDay - 0.5 + FrenchRepublicanCalendar.GREENWICH_PARIS_OFFSET_DAYS) );
    }
}
