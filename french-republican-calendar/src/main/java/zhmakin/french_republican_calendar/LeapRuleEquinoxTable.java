//
// Copyright (c) an 225 (2017 CE) Andrey Zhmakin
//

package zhmakin.french_republican_calendar;

import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;


/**
 * Leap rule calculation using equinox table from the resources.
 * @author Andrey Zhmakin
 */
class LeapRuleEquinoxTable
    implements FrenchRepublicanCalendar.LeapRule
{
    private final static LeapRuleEquinoxTable instance = new LeapRuleEquinoxTable();

    private static FrenchRepublicanCalendar.LeapRule fallbackLeapRule = FrenchRepublicanCalendar.LEAP_RULE_4_128RD;

    static int[] AUTUMNAL_EQUINOX_DAY;


    static
    {
        try
        {
            ClassLoader classLoader = LeapRuleEquinoxTable.class.getClassLoader();
            URL resourceURL = classLoader.getResource("zhmakin/french_republican_calendar/equinox_table.txt");

            Scanner scanner = new Scanner(resourceURL.openStream());

            List<Integer> buffer = new LinkedList<>();

            while (scanner.hasNextLine())
            {
                String line = scanner.nextLine();

                if (line.trim().isEmpty())
                {
                    continue;
                }

                Scanner lineScanner = new Scanner(line);

                double equinoxTime = lineScanner.nextDouble();

                int equinoxDay = FrenchRepublicanCalendar.julianToEquinoxDay(equinoxTime);

                buffer.add(equinoxDay);
            }

            AUTUMNAL_EQUINOX_DAY = new int[buffer.size()];

            int i = 0;

            for (Integer day : buffer)
            {
                AUTUMNAL_EQUINOX_DAY[i++] = day;
            }
        }
        catch (Throwable e)
        {
            e.printStackTrace();
        }
    }


    private LeapRuleEquinoxTable()
    {
        // Do nothing!
    }


    public static LeapRuleEquinoxTable getInstance()
    {
        return LeapRuleEquinoxTable.instance;
    }


    @Override
    public boolean isLeapYear(int year)
    {
        if (year >= 1 && year < AUTUMNAL_EQUINOX_DAY.length)
        {
            int daysInYear = AUTUMNAL_EQUINOX_DAY[year] - AUTUMNAL_EQUINOX_DAY[year - 1];

            assert daysInYear == 365 || daysInYear == 366 : "daysInYear = " + daysInYear;

            return daysInYear == 366;
        }
        else
        {
            return fallbackLeapRule.isLeapYear(year);
        }
    }
}
