//
// Copyright (c) an 225 (2017 CE) Andrey Zhmakin
//

package zhmakin.french_republican_calendar;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * Loads names of months and days from the XML file.
 * @author Andrey Zhmakin
 */
public class NameReader
{
    private Document doc;


    public NameReader(String resource)
        throws ParserConfigurationException,
            IOException,
            SAXException
    {
        ClassLoader classLoader = LeapRuleEquinoxTable.class.getClassLoader();
        URL resourceURL = classLoader.getResource(resource);

        InputStream inputStream = resourceURL.openStream();

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        this.doc = dBuilder.parse(inputStream);
    }


    public String getSeasonName(int season)
    {
        Element seasonElement = this.getSeasonElement(season);

        if (seasonElement == null)
        {
            return null;
        }

        Element nameElement = (Element) seasonElement.getElementsByTagName("name").item(0);

        return nameElement.getFirstChild().getNodeValue();
    }


    public String getMonthName(int month)
    {
        Element monthElement = this.getMonthElement(month);

        if (monthElement == null)
        {
            return null;
        }

        Element nameElement = (Element) monthElement.getElementsByTagName("name").item(0);

        return nameElement.getFirstChild().getNodeValue();
    }


    public String getDayName(int month, int dayOfMonth)
    {
        Element monthName = this.getMonthElement(month);

        if (monthName == null)
        {
            return null;
        }

        Node daysNode = monthName.getElementsByTagName("days").item(0);

        NodeList dayList = daysNode.getChildNodes();

        for (int i = 0; i < dayList.getLength(); i++)
        {
            Node dayNode = dayList.item(i);

            if (dayNode instanceof Element)
            {
                Element dayElement = (Element) dayNode;

                if (Integer.parseInt(dayElement.getAttribute("no")) == dayOfMonth)
                {
                    return dayElement.getFirstChild().getNodeValue();
                }
            }
        }

        return null;
    }


    public String getDayName(int dayOfYear)
    {
        assert dayOfYear >= 1 && dayOfYear <= 366;

        int month = (dayOfYear - 1) / 30;
        int dayOfMonth = (dayOfYear - 1) % 30 + 1;

        return this.getDayName(month, dayOfMonth);
    }


    public String getDayOfDecadeName(int dayOfDecade)
    {
        Element decadeDayElement = this.getDayOfDecadeElement(dayOfDecade);

        return decadeDayElement == null ? null : decadeDayElement.getFirstChild().getNodeValue();
    }


    private Element getDayOfDecadeElement(int dayOfDecade)
    {
        NodeList decadeDayList = this.doc.getDocumentElement().getElementsByTagName("decade").item(0).getChildNodes();

        for (int i = 0; i < decadeDayList.getLength(); i++)
        {
            Node node = decadeDayList.item(i);

            if (node instanceof Element)
            {
                Element seasonElement = (Element) node;

                if (Integer.parseInt(seasonElement.getAttribute("no")) == dayOfDecade)
                {
                    return seasonElement;
                }
            }
        }

        return null;
    }


    private Element getSeasonElement(int season)
    {
        NodeList seasonList = this.doc.getDocumentElement().getElementsByTagName("seasons").item(0).getChildNodes();

        for (int i = 0; i < seasonList.getLength(); i++)
        {
            Node node = seasonList.item(i);

            if (node instanceof Element)
            {
                Element seasonElement = (Element) node;

                if (Integer.parseInt(seasonElement.getAttribute("no")) == season)
                {
                    return seasonElement;
                }
            }
        }

        return null;
    }


    private Element getMonthElement(int month)
    {
        int season = (month == FrenchRepublicanCalendar.SANSCULOTTIDES) ? 3 : month / 3;

        Element seasonElement = this.getSeasonElement(season);

        if (seasonElement == null)
        {
            return null;
        }

        Node monthsElement = seasonElement.getElementsByTagName("months").item(0);

        NodeList monthList = monthsElement.getChildNodes();

        for (int j = 0; j < monthList.getLength(); j++)
        {
            Node monthNode = monthList.item(j);

            if (monthNode instanceof Element)
            {
                Element monthElement = (Element) monthNode;

                if (Integer.parseInt(monthElement.getAttribute("no")) == month)
                {
                    return monthElement;
                }
            }
        }

        return  null;
    }
}
