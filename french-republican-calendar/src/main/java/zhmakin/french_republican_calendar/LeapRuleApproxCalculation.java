//
// Copyright (c) an 225 (2017 CE) Andrey Zhmakin
//

package zhmakin.french_republican_calendar;

import zhmakin.astronomical_algorithms.meeus.EquinoxCalc;


/**
 * Leap rule approximate calculation on the fly.
 * @author Andrey Zhmakin
 */
public class LeapRuleApproxCalculation
    implements FrenchRepublicanCalendar.LeapRule
{
    private static LeapRuleApproxCalculation instance = new LeapRuleApproxCalculation();


    private LeapRuleApproxCalculation()
    {

    }


    @Override
    public boolean isLeapYear(int year)
    {
        int daysInYear
            = FrenchRepublicanCalendar.julianToEquinoxDay( EquinoxCalc.autumnalEquinox(year + 1792) )
                - FrenchRepublicanCalendar.julianToEquinoxDay( EquinoxCalc.autumnalEquinox(year + 1791) );

        assert daysInYear == 365 || daysInYear == 366 : "daysInYear = " + daysInYear;

        return daysInYear == 366;
    }


    public static LeapRuleApproxCalculation getInstance()
    {
        return instance;
    }
}
