# README #

### What is this repository for? ###

This is an implementation of the French Republican Calendar. The implementation extends java.util.Calendar and tries to follow conventions the Calendar class establishes.

*The implementation is incomplete.* Namely add() and roll() methods are not implemented.

### How do I get set up? ###

The 'astronomical-calculations' module implements autumnal equinox calculation rules found in Astronomical Algorithms, 1st edition (1991) by Jean Meeus.

The 'french-republican-calendar' module implements the calendar. Multiple leap rules are provided, including two real equinox data based rules.

FrenchRepublicanCalendarTest provides some JUnit tests for the calendar implementation.

### Who do I talk to? ###

Andrey Zhmakin
