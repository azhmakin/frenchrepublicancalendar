//
// Copyright (c) 2017 Andrey Zhmakin
//

package zhmakin.astronomical_algorithms.meeus;

/**
 * This class represents date and time, time of the day is expressed as a fractional part of the day.
 * @author Andrey Zhmakin
 */
public class Date
{
    /**
     * Stores year.
     */
    private int Y;

    /**
     * Stores month of the year (1-12).
     */
    private int M;

    /**
     * Stores day of month; Fractional part is time of the current date.
     */
    private double D;


    public Date(int Y, int M, double D)
    {
        this.Y = Y;
        this.M = M;
        this.D = D;
    }


    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof Date))
        {
            return false;
        }

        Date that = (Date) obj;

        return this.Y == that.Y
            && this.M == that.M
            && this.D == that.D;
    }


    /**
     * Converts internal state into readable string.
     * @return Date and time in format Year/MM/DD hh:mm:ss.
     */
    public String toDateAndTime()
    {
        int iDay = (int) Math.round(Math.floor(this.D));
        double fTime = this.D - iDay;

        int seconds = (int) Math.round((24 * 60 * 60) * fTime);
        int hour = seconds / (60 * 60);
        int minute = (seconds - hour * 60 * 60) / 60;
        int second = seconds - (hour * 60 + minute) * 60;

        return this.Y + "/"
            + String.format("%02d", this.M) + "/"
            + String.format("%02d", iDay) + " "
            + String.format("%02d", hour) + ":"
            + String.format("%02d", minute) + ":"
            + String.format("%02d", second);
    }


    @Override
    public String toString()
    {
        return this.Y + "." + this.M + "." + this.D;
    }
}
