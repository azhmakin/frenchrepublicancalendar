//
// Copyright (c) 2017 Andrey Zhmakin
//

package zhmakin.astronomical_algorithms.meeus;

/**
 * This class contains routines to compute autumnal equinox date for the given year.
 * The algorithm implemented is taken from Astronomical Algorithms, 1st edition (1991) by Jean Meeus, p.165.
 * @author Andrey Zhmakin
 */
public class EquinoxCalc
{
    private final static double[][] TABLE_26C =
        {
            {485, 324.96,   1934.136},
            {203, 337.23,  32964.467},
            {199, 342.08,     20.186},
            {182,  27.85, 445267.112},
            {156,  73.14,  45036.886},
            {136, 171.52,  22518.443},
            { 77, 222.54,  65928.934},
            { 74, 296.72,   3034.906},
            { 70, 243.58,   9037.513},
            { 58, 119.81,  33718.147},
            { 52, 297.17,    150.678},
            { 50,  21.02,   2281.226},
            { 45, 247.54,  29929.562},
            { 44, 325.15,  31555.956},
            { 29,  60.93,   4443.417},
            { 18, 155.12,  67555.328},
            { 17, 288.79,   4562.452},
            { 16, 198.04,  62894.029},
            { 14, 199.76,  31436.921},
            { 12,  95.39,  14577.848},
            { 12, 287.11,  31931.756},
            { 12, 320.81,  34777.259},
            {  9, 227.73,   1222.114},
            {  8,  15.45,  16859.074},
        };


    public static double autumnalEquinox(int year)
    {
        return calc( table_26_autumnalEquinox(year) );
    }


    private static double table_26_autumnalEquinox(int year)
    {
        assert year >= -1000 && year <= 3000;

        double jde0 = 0;

        if (year >= -1000 && year < 1000)
        {
            double y = year / 1000.0;

            double y2 = y * y;
            double y3 = y2 * y;
            double y4 = y2 * y2;

            jde0 = 1721325.70455
                + 365242.49558 * y
                - 0.11677 * y2
                - 0.00297 * y3
                + 0.00074 * y4;
        }
        else if (year >= 1000 && year <= 3000)
        {
            double y = (year - 2000) / 1000.0;

            double y2 = y * y;
            double y3 = y2 * y;
            double y4 = y2 * y2;

            jde0 = 2451810.21715
                + 365242.01767 * y
                - 0.11575 * y2
                + 0.00337 * y3
                + 0.00078 * y4;
        }

        return jde0;
    }


    public static double vernalEquinox(int year)
    {
        return calc( table_26_vernalEquinox(year) );
    }


    private static double table_26_vernalEquinox(int year)
    {
        assert year >= -1000 && year <= 3000;

        double jde0 = 0;

        if (year >= -1000 && year < 1000)
        {
            double y = year / 1000.0;

            double y2 = y * y;
            double y3 = y2 * y;
            double y4 = y2 * y2;

            jde0 = 1721139.29189
                + 365242.13740 * y
                - 0.06134 * y2
                - 0.00111 * y3
                + 0.00071 * y4;
        }
        else if (year >= 1000 && year <= 3000)
        {
            double y = (year - 2000) / 1000.0;

            double y2 = y * y;
            double y3 = y2 * y;
            double y4 = y2 * y2;

            jde0 = 2451623.80984
                + 365242.37404 * y
                - 0.05169 * y2
                + 0.00411 * y3
                + 0.00057 * y4;
        }

        return jde0;
    }


    public static double summerSolstice(int year)
    {
        return calc( table_26_summerSolstice(year) );
    }


    private static double table_26_summerSolstice(int year)
    {
        assert year >= -1000 && year <= 3000;

        double jde0 = 0;

        if (year >= -1000 && year < 1000)
        {
            double y = year / 1000.0;

            double y2 = y * y;
            double y3 = y2 * y;
            double y4 = y2 * y2;

            jde0 = 1721233.25401
                + 365241.72562 * y
                - 0.05323 * y2
                + 0.00907 * y3
                + 0.00025 * y4;
        }
        else if (year >= 1000 && year <= 3000)
        {
            double y = (year - 2000) / 1000.0;

            double y2 = y * y;
            double y3 = y2 * y;
            double y4 = y2 * y2;

            jde0 = 2451716.56767
                + 365241.62603 * y
                + 0.00325 * y2
                + 0.00888 * y3
                - 0.00030 * y4;
        }

        return jde0;
    }


    public static double winterSolstice(int year)
    {
        return calc( table_26_winterSolstice(year) );
    }


    private static double table_26_winterSolstice(int year)
    {
        assert year >= -1000 && year <= 3000;

        double jde0 = 0;

        if (year >= -1000 && year < 1000)
        {
            double y = year / 1000.0;

            double y2 = y * y;
            double y3 = y2 * y;
            double y4 = y2 * y2;

            jde0 = 1721414.39987
                + 365242.88257 * y
                - 0.00769 * y2
                + 0.00933 * y3
                + 0.00006 * y4;
        }
        else if (year >= 1000 && year <= 3000)
        {
            double y = (year - 2000) / 1000.0;

            double y2 = y * y;
            double y3 = y2 * y;
            double y4 = y2 * y2;

            jde0 = 2451900.05952
                + 365242.74049 * y
                + 0.06223 * y2
                + 0.00823 * y3
                - 0.00032 * y4;
        }

        return jde0;
    }


    private static double calc(double jde0)
    {
        double t = (jde0 - 2451545.0) / 36525;

        double w = 35999.373 * t - 2.47;

        double deltaLambda = 1
            + 0.0334 * cos(w)
            + 0.0007 * cos(2.0 * w);

        double s = 0;

        for (int i = 0; i < 24; i++)
        {
            s += TABLE_26C[i][0] * cos( TABLE_26C[i][1] + TABLE_26C[i][2] * t );
        }

        return jde0 + 0.00001 * s / deltaLambda;
    }


    private static double cos(double degrees)
    {
        return Math.cos(Math.toRadians(degrees));
    }
}
