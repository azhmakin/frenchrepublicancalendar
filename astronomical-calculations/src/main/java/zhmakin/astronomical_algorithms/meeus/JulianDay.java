//
// Copyright (c) 2017 Andrey Zhmakin
//

package zhmakin.astronomical_algorithms.meeus;


/**
 * Contains routines to convert calendar dates to Julian days and back.
 * The algorithms implemented are taken from Astronomical Algorithms, 1st edition (1991) by Jean Meeus, p.61.
 * @author Andrey Zhmakin
 */
public class JulianDay
{
    /**
     * Converts the date of Gregorian calendar into Julian day; Proleptic Gregorian calendar is forced.
     * @param Y Year.
     * @param M Month (1-12).
     * @param D Day of month (with decimals).
     * @return Julian day for the date passed.
     */
    public static double fromGregorian(int Y, int M, double D)
    {
        return convert(Y, M, D, true);
    }


    /**
     * Converts the date of Gregorian calendar into Julian day; Proleptic Gregorian calendar is forced.
     * @param Y Year.
     * @param M Month (1-12).
     * @param D Day of month.
     * @param hour Hour.
     * @param minute Minute.
     * @return Julian day for the date passed.
     */
    public static double fromGregorian(int Y, int M, int D, int hour, int minute)
    {
        return convert(Y, M, D + (hour * 60.0 + minute) / (24.0 * 60.0), true);
    }


    /**
     * Converts the date of Gregorian calendar into Julian day; Proleptic Gregorian calendar is forced.
     * @param Y Year.
     * @param M Month (1-12).
     * @param D Day of month.
     * @param hour Hour.
     * @param minute Minute.
     * @param second Second.
     * @return Julian day for the date passed.
     */
    public static double fromGregorian(int Y, int M, int D, int hour, int minute, int second)
    {
        return convert(Y, M, D + (hour * 60.0 + minute + second / 60.0) / (24.0 * 60.0), true);
    }


    /**
     * Converts the date of Julian calendar into Julian day.
     * @param Y Year.
     * @param M Month (1-12).
     * @param D Day of month (with decimals).
     * @return Julian day for the date passed.
     */
    public static double fromJulian(int Y, int M, double D)
    {
        return convert(Y, M, D, false);
    }


    /**
     * Converts date into Julian day. Dates before the 4th of October 1582 (inclusive)
     * are considered dates of Julian calendar.
     * @param Y Year.
     * @param M Month (1-12).
     * @param D Day of month and time as a fractional part.
     * @return Julian day.
     */
    public static double convert(int Y, int M, double D)
    {
        boolean gregorian = false;

        if (Y > 1582)
        {
            gregorian = true;
        }
        else if (Y == 1582)
        {
            if (M > 10)
            {
                gregorian = true;
            }
            else if (M == 10)
            {
                // Oct 15 follows Oct 4
                // It could be "5.0", but then we may be sure that the date is Julian
                gregorian = D >= 15.0;
            }
        }

        return convert(Y, M, D, gregorian);
    }


    /**
     * Converts date into Julian day.
     * @param Y Year.
     * @param M Month (1-12).
     * @param D Day of month and time as a fractional part.
     * @param gregorian Whether to use Gregorian (including proleptic) of Julian calendar.
     * @return Julian day.
     */
    public static double convert(int Y, int M, double D, boolean gregorian)
    {
        assert Y >= -4712;
        assert M >= 1 && M <= 12;

        if (M == 1 || M == 2)
        {
            Y--;
            M += 12;
        }

        int B = 0;

        if (gregorian)
        {
            int A = INT(Y / 100.0);
            B = 2 - A + INT(A / 4.0);
        }

        double julianDay = INT(365.25 * (Y + 4716)) + INT(30.6001 * (M + 1)) + D + B - 1524.5;

        assert julianDay >= 0;

        return julianDay;
    }


    public static double modifiedJulianDay(int Y, int M, double D)
    {
        return convert(Y, M, D) - 2400000.5;
    }


    /**
     * Converts Julian day into date.
     * @param julianDay Julian date.
     * @return Date of Julian or Gregorian calendar.
     */
    public static Date toDate(double julianDay)
    {
        int Z = (int) Math.round(Math.floor(julianDay + 0.5));
        double F = (julianDay + 0.5) - Z;

        int A;

        if (Z < 2299161)
        {
            A = Z;
        }
        else
        {
            int alpha = INT((Z - 1867216.25) / 36524.25);
            A = Z + 1 + alpha - INT(alpha / 4.0);
        }

        int B = A + 1524;
        int C = INT((B - 122.1) / 365.25);
        int D = INT(365.25 * C);
        int E = INT((B - D) / 30.6001);

        double day = B - D - INT(30.6001 * E) + F;

        int M;

        if (E < 14)
        {
            M = E - 1;
        }
        else
        {
            assert  E == 14 || E == 15;

            M = E - 13;
        }

        int Y;

        if (M > 2)
        {
            Y = C - 4716;
        }
        else
        {
            assert  M == 1 || M == 2;

            Y = C - 4715;
        }

        return new Date(Y, M, day);
    }


    private static int INT(double value)
    {
        return (int) Math.round(Math.floor(value));
    }
}
