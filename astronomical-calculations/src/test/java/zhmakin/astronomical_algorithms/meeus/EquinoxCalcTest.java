//
// Copyright (c) 2017 Andrey Zhmakin
//

package zhmakin.astronomical_algorithms.meeus;

import org.junit.Assert;
import org.junit.Test;


/**
 * Tests equinox and solstice calculation.
 * @author Andrey Zhmakin
 */
public class EquinoxCalcTest
{
    @Test
    public void test_01()
    {
        // 23 September 2019, 07:50 UTC

        double actual = JulianDay.fromGregorian(2019, 9, 23, 7, 50);
        //System.out.println("actual     = " + actual);

        double calculated = EquinoxCalc.autumnalEquinox(2019);
        //System.out.println("calculated = " + calculated);

        Assert.assertTrue( Math.abs(actual - calculated) <= 120.0 / (24 * 60 * 60) );

        //Date date = JulianDay.toDate(calculated);

        //System.out.println(date.toDateAndTime());
    }


    @Test
    public void test_02()
    {
        // 20 March 2019, 21:59 UTC

        double actual = JulianDay.fromGregorian(2019, 3, 20, 21, 59);
        //System.out.println("actual     = " + actual);

        double calculated = EquinoxCalc.vernalEquinox(2019);
        //System.out.println("calculated = " + calculated);

        Assert.assertTrue( Math.abs(actual - calculated) <= 120.0 / (24 * 60 * 60) );

        //Date date = JulianDay.toDate(calculated);

        //System.out.println(date.toDateAndTime());
    }


    @Test
    public void test_03()
    {
        // 21 June 1962, 21:25:08 UTC

        double actual = JulianDay.fromGregorian(1962, 6, 21, 21, 25);
        System.out.println("actual     = " + actual);

        double calculated = EquinoxCalc.summerSolstice(1962);
        System.out.println("calculated = " + calculated);

        Assert.assertTrue( Math.abs(actual - calculated) <= 120.0 / (24 * 60 * 60) );

        //Date date = JulianDay.toDate(calculated);

        //System.out.println(date.toDateAndTime());
    }


    @Test
    public void test_04()
    {
        // 21 June 1962, 21:25:08 UTC

        double actual = JulianDay.fromGregorian(2019, 12, 22, 4, 19);
        System.out.println("actual     = " + actual);

        double calculated = EquinoxCalc.winterSolstice(2019);
        System.out.println("calculated = " + calculated);

        Assert.assertTrue( Math.abs(actual - calculated) <= 120.0 / (24 * 60 * 60) );

        //Date date = JulianDay.toDate(calculated);

        //System.out.println(date.toDateAndTime());
    }
}
