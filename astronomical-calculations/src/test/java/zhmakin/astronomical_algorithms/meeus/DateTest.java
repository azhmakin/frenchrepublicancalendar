//
// Copyright (c) 2017 Andrey Zhmakin
//

package zhmakin.astronomical_algorithms.meeus;

import org.junit.Assert;
import org.junit.Test;

public class DateTest
{
    @Test
    public void test_01()
    {
        Assert.assertTrue(new Date(2017, 1, 1.5).toDateAndTime().equals("2017/01/01 12:00:00"));
    }
}
