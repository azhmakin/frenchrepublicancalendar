//
// Copyright (c) an 226 (2018 CE) Andrey Zhmakin
//

package zhmakin.decimal_clock;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.time.LocalTime;
import java.time.ZoneId;

public class JDecimalClockWidget
    extends JPanel
{
    private Timer timer;
    private BufferedImage dial;


    public JDecimalClockWidget()
    {
        this.timer = new Timer(0, e -> JDecimalClockWidget.this.repaint());

        this.timer.setDelay(100);

        this.addAncestorListener(new AncestorListener() {
            @Override
            public void ancestorAdded(AncestorEvent event)
            {
                JDecimalClockWidget.this.timer.start();
            }

            @Override
            public void ancestorRemoved(AncestorEvent event)
            {
                JDecimalClockWidget.this.timer.stop();
            }

            @Override
            public void ancestorMoved(AncestorEvent event) { }
        });

        try
        {
            this.dial = ImageIO.read(this.getClass().getResource("/dial.png"));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }


    @Override
    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);

        Graphics2D g2 = (Graphics2D) g;

        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
            RenderingHints.VALUE_ANTIALIAS_ON);

        int width = this.getWidth();
        int height = this.getHeight();

        int dim = Math.min(width, height);

        int centreX = width / 2;
        int centreY = height / 2;

        // Draw dial
        {
            g.drawImage(this.dial, (width - dim) / 2, (height - dim) / 2, dim, dim, null);
        }

        LocalTime time = LocalTime.now(ZoneId.systemDefault());

        int millisecond = millisecondOfTime(time);

        int[] tm = millisecondOfDayToDecimalTime(millisecond);

        JDecimalClock.decimalClock.setTitle(timeToString(tm));

        // Draw hour hand

        {
            double angle = (Math.PI / 2.0) + (tm[0] + tm[1] / 100.0) * (2.0 * Math.PI) / 10.0;

            int x = (int) Math.round( dim * Math.cos(angle) * 0.5 );
            int y = (int) Math.round( dim * Math.sin(angle) * 0.5 );

            x = (x * 50) / 100;
            y = (y * 50) / 100;

            //System.out.println(x + "\t" + y);
            g.setColor(Color.BLACK);
            g.drawLine(centreX, centreY, centreX - x, centreY - y);
        }

        // Draw minute hand

        {
            double angle = (Math.PI / 2.0) + (tm[1] + tm[2] / 100.0) * (2.0 * Math.PI) / 100.0;

            int x = (int) Math.round( dim * Math.cos(angle) * 0.5 );
            int y = (int) Math.round( dim * Math.sin(angle) * 0.5 );

            x = (x * 70) / 100;
            y = (y * 70) / 100;

            //System.out.println(x + "\t" + y);
            g.setColor(Color.BLACK);
            g.drawLine(centreX, centreY, centreX - x, centreY - y);
        }

        // Draw second hand

        {
            double angle = (Math.PI / 2.0) + (tm[2] + tm[3] / 1000.0) * (2.0 * Math.PI) / 100.0;

            int x = (int) Math.round( dim * Math.cos(angle) * 0.5 );
            int y = (int) Math.round( dim * Math.sin(angle) * 0.5 );

            x = (x * 90) / 100;
            y = (y * 90) / 100;

            //System.out.println(x + "\t" + y);
            g.setColor(Color.RED);
            g.drawLine(centreX, centreY, centreX - x, centreY - y);
        }
    }


    static int millisecondOfTime(LocalTime time)
    {
        return ((time.getHour() * 60 + time.getMinute()) * 60 + time.getSecond()) * 1000 + time.getNano() / 1_000_000;
    }


    static int[] millisecondOfDayToDecimalTime(int millisecond)
    {
        int dms = (int)( millisecond * 10_00_00_000L / (24 * 60 * 60 * 1000L) );

        int sec = (dms / 1000) % 100;
        int min = (dms / 1000_00) % 100;
        int hrs = (dms / 1000_00_00)/* % 10*/;

        return new int[] { hrs, min, sec, dms % 1000 };
    }


    static String timeToString(int[] time)
    {
        StringBuilder builder = new StringBuilder();

        //tm[0] + ":" + tm[1] + ":" + tm[2];

        builder.append(time[0]);

        builder.append(':');

        if (time[1] < 10)
        {
            builder.append('0');
        }

        builder.append(time[1]);

        builder.append(':');

        if (time[2] < 10)
        {
            builder.append('0');
        }

        builder.append(time[2]);

        return builder.toString();
    }
}
