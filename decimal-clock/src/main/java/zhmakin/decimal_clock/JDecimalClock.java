//
// Copyright (c) an 226 (2018 CE) Andrey Zhmakin
//

package zhmakin.decimal_clock;

import javax.swing.*;
import java.awt.*;

public class JDecimalClock
    extends JFrame
{
    public JDecimalClock()
    {
        super("DecimalClock");

        this.setSize(250, 250);
        this.setLocationRelativeTo(null);

        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        Container contentPane = this.getContentPane();

        contentPane.add(new JDecimalClockWidget(), BorderLayout.CENTER);

        this.setVisible(true);
    }


    static JDecimalClock decimalClock;


    public static void main(String[] args)
    {
        decimalClock = new JDecimalClock();
    }
}
